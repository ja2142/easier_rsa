# Easier RSA

### Even easier then easy RSA

An easy-rsa based tool to generate complete one file openvpn connection packs with one command.

Requires installed openvpn to generate tls auth key.

## Usage

### Generate server config (do once):

Clone with submodules:

```
git clone --recurse-submodules <cloned-url>
```

Generate pki and server configuration

```
./easier_rsa.py server_gen
```

Modify configurations, if needed (`server_out/server.conf` and `skel/conf.ovpn`).

Openvpn server can be run with configuration in `server_out`, either directly:

```
cd server_out && sudo openvpn server.conf
```

Or for anything more long-term, config can be taken to a server, and put in a config location (probably something like `/etc/openvpn/server`) and run as a service.

### Generate client config (for each client):

For each client run:

```
./easier_rsa.py client_gen <client_name>
```

This will generate .ovpn connection pack in `client_out/`.

For example:

```
./easier_rsa.py client_gen garry
```

will generate `client_out/garry.ovpn`. Send it to Garry, he should be able to connect with:

```
sudo openvpn garry.ovpn
```
