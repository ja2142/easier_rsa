#!/usr/bin/env python3

import argparse
import os
import shutil
import subprocess

EASYRSA_DIR = 'easy-rsa/easyrsa3'
EASYRSA_EXEC = f'{EASYRSA_DIR}/easyrsa'
PKI_DIR = 'pki'

def gen_client_cert(cn):
    print(f'generating request for {cn}')
    if subprocess.run(f'{EASYRSA_EXEC} gen-req {cn} nopass batch -req-cn {cn}', shell=True).returncode != 0:
        print('failed to generate signing request')
        exit()

    print(f'signing cert for {cn}')
    if subprocess.run(f'echo yes | {EASYRSA_EXEC} sign-req client {cn}', shell=True).returncode != 0:
        print('failed to sign certificate')
        exit()

def gen_client_config(cn):
    print('generating client conf')

    os.makedirs('client_out', mode=0o700, exist_ok=True)

    with open('skel/conf.ovpn') as skel_file:
        ovpn_skel = skel_file.read()

    cert_filename = f'{PKI_DIR}/issued/{cn}.crt'
    key_filename = f'{PKI_DIR}/private/{cn}.key'
    ta_filename = 'server_out/ta.key'
    ca_cert_filename = f'{PKI_DIR}/ca.crt'

    if not os.path.isfile(ca_cert_filename):
        print('ca doesn\'t exist, generate server config first')
        exit()

    if not os.path.isfile(cert_filename) or not os.path.isfile(key_filename):
        print('client doesn\'t exist, generating and signing')
        gen_client_cert(cn)

    with open(cert_filename, 'r') as cert_file:
        client_cert = cert_file.read()

    with open(key_filename, 'r') as key_file:
        client_key = key_file.read()

    with open('pki/ca.crt', 'r') as ca_cert_file:
        ca_cert = ca_cert_file.read()

    if os.path.isfile(ta_filename):
        with open('server_out/ta.key', 'r') as ta_file:
            ta_key = ta_file.read()
    else:
        ta_key = None
        print('tls auth key doesn\'t exist, skipping')
    
    with open(f'client_out/{cn}.ovpn', 'w') as outfile:
        outfile.write(ovpn_skel)

        outfile.write('<ca>\n')
        outfile.write(ca_cert)
        outfile.write('</ca>\n')

        outfile.write('<cert>\n')
        outfile.write(client_cert)
        outfile.write('</cert>\n')

        outfile.write('<key>\n')
        outfile.write(client_key)
        outfile.write('</key>\n')
        if ta_key:
            outfile.write('key-direction 1\n')
            outfile.write('<tls-auth>\n')
            outfile.write(ta_key)
            outfile.write('</tls-auth>\n')

def gen_server_config(hard_reset):
    # TODO safeguard against accidental running (check if ca already exists, etc)
    # (although init-pki gives a big fat warning already, and asks for confirmation)

    init_pki_opt = 'hard-reset' if hard_reset else ''
    if subprocess.run(f'{EASYRSA_EXEC} init-pki {init_pki_opt}', shell=True).returncode != 0:
        print('failed to generate initialize pki')
        exit()

    if subprocess.run(f'{EASYRSA_EXEC} build-ca nopass', shell=True).returncode != 0:
        print('failed to generate ca')
        exit()

    if subprocess.run(f'{EASYRSA_EXEC} gen-req server nopass batch -req-cn server', shell=True).returncode != 0:
        print('failed to generate server cert request')
        exit()

    if subprocess.run(f'echo yes | {EASYRSA_EXEC} sign-req server server', shell=True).returncode != 0:
        print('failed to sign server cert')
        exit()

    if subprocess.run(f'{EASYRSA_EXEC} gen-dh', shell=True).returncode != 0:
        print('failed to generate DH parameters')
        exit()

    if hard_reset:
        shutil.rmtree('server_out', ignore_errors=True)
        shutil.rmtree('client_out', ignore_errors=True)
        os.makedirs('server_out', mode=0o700)

    if subprocess.run('openvpn --genkey secret server_out/ta.key', shell=True).returncode != 0:
        print('failed to generate tls auth key (if you plan on using them)')

    shutil.copy(f'{PKI_DIR}/ca.crt', 'server_out')
    shutil.copy(f'{PKI_DIR}/private/server.key', 'server_out')
    shutil.copy(f'{PKI_DIR}/issued/server.crt', 'server_out')
    shutil.copy(f'{PKI_DIR}/dh.pem', 'server_out')
    shutil.copy('skel/server.conf', 'server_out')

    print('done')
    print('adjust configs as needed (server_out/server.conf and skel/conf.ovpn), then generate clients with:')
    print('./easier_rsa client_gen <client_name>')

def main():
    parser = argparse.ArgumentParser(description='limited, but easy to use openvpn client config generator based on easy-rsa')
    subparsers = parser.add_subparsers(dest='command', required=True)

    client_gen_parser = subparsers.add_parser('client_gen')
    client_gen_parser.add_argument('client_name', help='common name of a client to generate connection pack for')

    server_gen_parser = subparsers.add_parser('server_gen')
    server_gen_parser.add_argument('--hard-reset', help='perform hard pki reset (completely remove pki directory, pass hard-reset to easyrsa)', action='store_true')

    args = vars(parser.parse_args())

    match args['command']:
        case 'client_gen':
            gen_client_config(args['client_name'])
        case 'server_gen':
            gen_server_config(args['hard_reset'])

if __name__ == '__main__':
    main()

